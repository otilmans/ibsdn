#!/usr/bin/env python

import sys, os

outdir = "stretch_stats"

def print_usage():
	print "usage: %s <input-file> <topology>" %(sys.argv[0])
	sys.exit(1)

def write_list_to_file(lst,filename):
	print filename
	string = ""
	for e in lst:
		string += "%s\n" %(e)
	f=open(filename,'w+')
	f.write(string)
	f.close()

def get_path_after_rmuturn(ibsdn_path):
	fin_ibsdn_path = []
	for node in ibsdn_path:
		if node in fin_ibsdn_path:
			nodeind = fin_ibsdn_path.index(node)
			fin_ibsdn_path = fin_ibsdn_path[:nodeind] + [node]
		else:
			fin_ibsdn_path.append(node)
	return fin_ibsdn_path

def get_maximal_uturns(ibsdn_path,nouturn_path):
	uturns = {}
	reverse_ibsdn = list(ibsdn_path)
	reverse_ibsdn.reverse()
	for node in nouturn_path:
		first_index = ibsdn_path.index(node)
		last_index = len(reverse_ibsdn) - 1 - reverse_ibsdn.index(node)
		if first_index != last_index:
			#print "%s,%s,%s,%s,%s" %(ibsdn_path,nouturn_path,node,first_index,last_index)
			uturns[str(ibsdn_path[first_index:last_index+1])] = last_index - first_index
	return uturns

if len(sys.argv) < 3:
	print_usage()
fname = sys.argv[1]
topo = sys.argv[2]
if not os.path.isfile(fname):
	print_usage()
infile = open(fname)
stretch = []
stretch_no_uturn = []
uturns = []
for l in infile.readlines():
	if l.startswith("F"):
		continue
	paths = l.split(";")
	paths.pop(0)
	for f in range(0,len(paths)/2,2):
		failure_id = "%s%s" %(hash(l),f)
		fin_path = paths[f].split(" ")
		ibsdn_path = paths[f+1].split(" ")
		ibsdn_path_rmuturn = get_path_after_rmuturn(ibsdn_path)
		uturns_map = get_maximal_uturns(ibsdn_path,ibsdn_path_rmuturn)
		stretch.append("%s\t%s\t%s" %(len(ibsdn_path) - len(fin_path),ibsdn_path,fin_path))
		stretch_no_uturn.append("%s\t%s\t%s" %(len(ibsdn_path_rmuturn) - len(fin_path),ibsdn_path_rmuturn,fin_path))
		if len(ibsdn_path) == len(ibsdn_path_rmuturn):
			uturns.append("%s\t%s\t%s" %(0,[],failure_id))
		else:
			for u in uturns_map:
				uturns.append("%s\t%s\t%s" %(uturns_map[u],u,failure_id))
infile.close()

#write_list_to_file(stretch,"%s/%s_simple_stretch.dat" %(outdir,topo))
#write_list_to_file(stretch_no_uturn,"%s/%s_nouturn_stretch.dat" %(outdir,topo))
write_list_to_file(uturns,"%s/%s_uturns.dat" %(outdir,topo))

