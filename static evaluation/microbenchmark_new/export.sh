#!/bin/sh
cd $1
FILES10=(10f*.csv)
FILES100=(100f*.csv)
FILES1000=(1000f*.csv)
echo "${FILES10[@]}"
Rscript ../variation.R ${FILES10[@]}
Rscript ../variation.R ${FILES100[@]}
Rscript ../variation.R ${FILES1000[@]}
