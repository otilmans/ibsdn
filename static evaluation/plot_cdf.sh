#!/bin/bash

outdir="."

# read arguments
function usage(){
cat << EOF
usage: $0 [<options>]

This script plots results as CCDFs.

OPTIONS:
   -h      Show this message
   -l      Plots graphs with x-axis in log scale
   -p	   Plots points
   -r	   Set the result directory to be plotted (default is $basedir)
   -f	   Use a custom egrep filter to exclude some results
   -x	   Fix tick and labeling (10,50,100,500,1k,5k,10k,50k) on the X axis
EOF
exit 1
}

basedir="/Users/ste/OngoingWork/git/ibs/experiments/rocketfuel-topologies/all_bck_entries/"
log=""
points="FALSE"
filter="dat$"
xaxis=""
while getopts hlpxr:f: OPTION
do
     case $OPTION in
         h)
             usage
             ;;
         l)
             log="log=\"x\","
             ;;
		 r)
	     	 basedir=$OPTARG
	     	 ;;
     	 p)
			 points="TRUE"
			 ;;
	     f)
			 filter=$OPTARG
			 ;;
		 x)
			 #xaxis="axis(1,at=c(10,50,100,500,1000,5000,10000,50000),labels=c(\"10\",\"50\",\"100\",\"500\",\"1k\",\"5k\",\"10k\",\"50k\"))"
			 xaxis="axis(1,at=c(10,100,1000,10000,100000,1000000),labels=c(\"10\",\"100\",\"1k\",\"10k\",\"100k\",\"1M\"))"
			 ;;
	esac
done

files=$(ls $basedir)
echo $files
c="0"
labels=""

#min="0.1"
#max="10000"
echo "$basedir/$files"
min=$(cat $basedir/* | awk '{print $1}' | sort -n | head -n 1)
if [ $min == "0" ]; then
	min=0.1
fi 
#min=100
max=$(cat $basedir/* | awk '{print $1}' | sort -nr | head -n 1)
echo $min,$max
#if [ "$log" == "" ]; then
#	max=$(expr $max + $(eval "expr $max \* 25 / 100"))
#else
#	max=$(expr $max + $(eval "expr $max \* 40 / 100"))
#fi
s=$s"\npostscript(\"ecdf.eps\", horizontal=FALSE, width=9, height=6)"
s=$s"\npar(cex.lab=1.2, cex.axis=1.2, cex=1)"
s=$s"\npar(mar=c(5,5,1,2))"

for f in $files; do
	analysis_id=$(echo $f | sed 's/\//-/g' | awk -F"\." '{print $(NF-1)}')
	echo $analysis_id
	
	# collect info for R
	s=$s"\ndata$c = read.table(\"$basedir/$f\",sep=\"\t\")"
	s=$s"\nprint(ecdf(data$c[,1]))"
	s=$s"\ni = $(( $c / 2 + 1 ))"
	#s=$s"\ncolor = $(( ${c} / 2 + 1))"
	s=$s"\ncolor = $(( ${c} + 1))"
	xlab="Packet return size"
	#xlab="number of FIB entries"
	if [ "$log" != "" ]; then
		xlab=$xlab" (log scale)"
	fi
	#ylab="CDF of nodes"
	ylab="CDF of paths"
	if [ "$c" == "0" ]; then
		if [ "$xaxis" != "" ]; then
			s=$s"\nplot(ecdf(data$c[,1]), xlab=\"$xlab\", ylab=\"$ylab\", xlim=c($min,$max), main=\"\", $log xaxt=\"n\", col.points=color, verticals=TRUE, col.hor=color, col.ver=color, do.points=$points, pch=$c, lty=$(( $c + 1 )))"
			s=$s"\n"$xaxis
		else
			s=$s"\nplot(ecdf(data$c[,1]), xlab=\"$xlab\", ylab=\"$ylab\", xlim=c($min,$max), main=\"\", $log col.points=color, verticals=TRUE, col.hor=color, col.ver=color, do.points=$points, pch=$c, lty=$(( $c + 1 )))"
			#s=$s"\nplot(ecdf(data$c[,1]), xlab=\"number of FIB entries\", ylab=\"CDF of nodes\", xlim=c($min,$max), main=\"\", $log col.points=color, verticals=TRUE, col.hor=color, col.ver=color, do.points=$points, pch=$c, lty=$(( $c % 2 + 1)))"
		fi
	else
		s=$s"\nplot(ecdf(data$c[,1]), add=TRUE, col.points=color, verticals=TRUE, col.hor=color, col.ver=color, do.points=$points, pch=$c, lty=$(( $c + 1)))"
			#lty=$(( $c % 2 + 1)))"
	fi
	newlabel=$(echo $analysis_id | sed 's/_.*//g')
	labels=$labels" "$newlabel
	c=$(expr $c + 1)
done
labels=$(echo $labels | sed 's/^/\"/g' | sed 's/$/\"/g' | sed 's/ /\",\"/g')
#colors=$(for i in $(seq 1 $(( $c / 2 )));do echo $i,$i,; done | tr -d "\n" | sed 's/,$//')
colors=$(for i in $(seq 1 $(( $c )));do echo $i,; done | tr -d "\n" | sed 's/,$//')
pchs=$(for i in $(seq 1 $c); do echo 0,; done | tr -d "\n" | sed 's/,$//')
ltys=$(for i in $(seq 1 $c); do echo $i,; done | tr -d "\n" | sed 's/,$//')
#ltys=$(for i in $(seq 1 $(( $c / 2 )));do echo 1,2,; done | tr -d "\n" | sed 's/,$//')
if [ "$points" == "TRUE" ]; then
	echo "with points!"
	pchs=$(echo $(seq 0 $(( c - 1 ))) | sed 's/ /,/g')
	s=$s"\nlegend(\"bottomright\", c($labels), col=c($colors), lty=c($ltys), pch=c($pchs), cex=1)"
else
	echo "without points!"
	s=$s"\nlegend(\"bottomright\", c($labels), col=c($colors), lty=c($ltys), cex=1.2)"
fi
echo -e $s

# use R to plot
Rscript=$(mktemp)

cat <<EOF > $Rscript

source("./lib.eccdf.R")
$(echo -e $s)

EOF

R --no-save < $Rscript
#rm $Rscript

