#!/usr/bin/env python
import sys
import signal
import datetime
import os
from math import sqrt
import controller.flow as flow
import cPickle as pickle
from compiler import rocketfuel

LOG_FILE = 'sim.log'
RESULT_DIR = 'raw_data'
ALLBCKS_DIR = 'all_bck_entries'
PERFAILURE_DIR = 'bck_entries_per_failure'


def write_list_to_file(lst,filename):
    print filename
    string = ""
    for e in lst:
        string += "%s\n" %(e)
    f=open(filename,'w+')
    f.write(string)
    f.close()

def perform_evaluation(benchmark_list):
    export_flowmods_per_failure(benchmark_list)
    for name, benchmark in benchmark_list:
        data = []
        toponame = name.split('/')[1]

        #generate plain files with all backup entries
        filename = "%s/%s-nobck.dat" %(ALLBCKS_DIR,toponame)
        write_list_to_file(benchmark.flows_per_router(count_backup=False).values(),filename)
        
        filename = "%s/%s-bck.dat" %(ALLBCKS_DIR,toponame)
        write_list_to_file(benchmark.flows_per_router(count_backup=True).values(),filename)

def export_flowmods_per_failure(benchmark_list):
    for name, benchmark in benchmark_list:
        toponame = name.split('/')[1]
        filename = "%s/%s-rulemods4failure.dat" %(PERFAILURE_DIR,toponame)
        write_list_to_file(benchmark.flowmods_per_failure().values(),filename)

        
def export_benchmark_paths(benchmark, name):
    """Helper for export_path_stretch"""
    mappings = {}
    def mapping_for(id):
        """Define a 1-1 mapping for hostname and integer"""
        if id not in mappings:
            mappings[id] = len(mappings)
        return mappings[id]
        
    def export_flow(flow):
        """Return a list giving each associated flow with this Flow:
        the originalflow,
        the igp path after the failure of the first link,
        the ibsdn path after the failure of the first link,
        2nd, 3rd, ...
        
        This returns a list of list
        """
        line = [[mapping_for(hop) for hop in flow.flow]]
        for i in range(0, len(flow.flow) - 1):
            link = (flow.flow[i], flow.flow[i + 1])
            if link in flow.backups:
                line.append([mapping_for(hop) for hop in flow.backups[link].backup_flow])
                line.append([mapping_for(hop) for hop in (flow.ibs_backups[link])])
        return line
        
    def write_line(f, line):
        """format the content of line as csv and write it in f""" 
        f.write('%s\n' % ';'.join([' '.join([str(e) for e in elem]) for elem in line]))
    
    length = 0
    lines = []
    for pair in benchmark.pairs.itervalues():
        for flow in pair.all_flows:
            export = export_flow(flow)
            length = max(len(export), length)
            lines.append(export)
    with open('%s_paths.csv' % name, 'w') as f:
        header = ['Flow']
        for i in range(0, length/2):
            header.append('IGP-F%s' % i)
            header.append('IBSDN-F%s' % (i))
        write_line(f, header)
        for line in lines:
            write_line(f, line)
    
def export_path_stretch(objects):
    """Export all path details about every flow in the networks 
    in csv files (flow, backup paths, ...)"""
    for name, benchmark in objects:
        log('Exporting paths for %s' % name)
        export_benchmark_paths(benchmark, name)
        
def update_object(obj):
    """Update operation for saved objects
    Use with caution"""
    for pair in obj.pairs.itervalues():
        for flow in pair.all_flows:
            flow.ibs_backups = {}
            for i in range(0, len(flow.flow) - 1):
                link = (flow.flow[i], flow.flow[i + 1])
                if link in flow.backups:
                    flow.ibs_backups[link] = flow.ibs_backup_for_index(i)
        
class Benchmark(object):
    def __init__(self, network):
        self.network = network # the network graph
        log('Computing all flows')
        self.pairs = flow.all_network_pairedflows(network)  # all disjoint flows per pair
        log('Computing all backup flows')
        for pairbenchmark in self.pairs.itervalues():
            pairbenchmark.compute_backups()

    def flows_per_router(self, count_backup=False):
        """Return a dictionnary giving the numbers of flow entries for each router/key
        If count_backup is True, also takes into account the entries needed for all backup flows"""
        flow_entries = {}
        for n in self.network:
            flow_entries[n] = 0
        for pairbenchmark in self.pairs.itervalues():
            # Explore all pairs
            for flowbenchmark in pairbenchmark.all_flows:
                # And check all installed flows
                for hop in flowbenchmark.flow:
                    flow_entries[hop] += 1
                if count_backup:
                    # Explore all backup flows
                    for backupflow in flowbenchmark.backups.itervalues():
                        for hop in backupflow.flow_mods():
                            flow_entries[hop] += 1
        return flow_entries

    def flowmods_per_failure(self):
        flow_mods = {}
        for pairbenchmark in self.pairs.itervalues():
            for flowbenchmark in pairbenchmark.all_flows:
                #print flowbenchmark.backups
                for edge in flowbenchmark.backups.keys():
                    reroute = flowbenchmark.backups[edge]
                    #print reroute, reroute.flow_mods()
                    if not flow_mods.has_key(edge):
                        flow_mods[edge] = 0
                    flow_mods[edge] += len(reroute.flow_mods()) - 1
                    # NOTE: the -1 is justified by the fact that the convergence point may not need to be modified! 
        return flow_mods

    def print_stats(self):
        """'Random' possibly meaningless stats"""
        log('--------------------------------------------------')
        log('Total node count: %d' % len(self.network))
        log('Total number of pairs: %d' % len(self.pairs))
        flow_count = 0
        for pair in self.pairs:
            flow_count += len(self.pairs[pair].all_flows)
        log('Total number of flows: %s' % flow_count)
        log('Mean number of flow entries per router')
        no_backup = self.flows_per_router(count_backup=False)
        no_backup_mean = dict_average(no_backup)
        log('\tWithout backup paths: %s' % no_backup_mean)
        log('\t\tStandard deviation: %s' % dict_std(no_backup_mean, no_backup))
        backups = self.flows_per_router(count_backup=True)
        backups_mean = dict_average(backups)
        log('\tWith backup paths:    %s' % backups_mean)
        log('\t\tStandard deviation: %s' % dict_std(backups_mean, backups))
        log('Minimal number of flow entries per router')
        log('\tWithout backup paths: %s' % min(no_backup.itervalues()))
        log('\tWith backup paths:    %s' % min(backups.itervalues()))
        log('Maximal number of flow entries per router')
        log('\tWithout backup paths: %s' % max(no_backup.itervalues()))
        log('\tWith backup paths:    %s' % max(backups.itervalues()))
        log('--------------------------------------------------')


## Boilerplate code

def dict_std(mean, d):
    """Returns the standard deviation of the values in the given dictionnary given a mean"""
    ssum = 0.0
    for val in d.itervalues():
        ssum += ((val - mean) * (val - mean))
    return sqrt(ssum / (len(d) - 1))


def dict_average(d):
    """Returns the average of the values in the given dictionnary"""
    return sum(d.itervalues()) / len(d)


def log(msg):
    time = '%s | ' % str(datetime.datetime.now().time())
    print time, msg
    if logfile:
        logfile.write(time)
        logfile.write(msg)
        logfile.write('\n')


def close_logfile():
    log('===== Session stop: %s' % str(datetime.datetime.now()))
    if logfile:
        logfile.close()


def cleanup(*args, **kwargs):
    # if called by a signal handler, it will have 2 args: sig number and stack frame
    if len(args) > 0:
        log('Caught signal, exiting ...')
    log('Cleaning up')
    close_logfile()
    sys.exit(0)


def save_object(results, picklename):
    log('Saving Benchmark object to %s' % picklename)
    with open(picklename, 'w') as picklefile:
        pickle.dump(results, picklefile)

def handle_path(topo, reload, update):
    name, ext = os.path.splitext(topo)
    picklename = '%s.pikle' % name
    # Resume previous experiment
    if reload and os.path.exists(picklename):
        log('Reloading the saved object in %s' % picklename)
        with open(picklename, 'r') as f:
            results = pickle.load(f)
        if update:
            log('Updating object')
            update_object(results)
            save_object(results, picklename)
    else:
        # Create new object from scratch, length process!
        log('Loading the weighted topology in %s' % topo)
        network_graph = rocketfuel.load_weighted_igp(topo)
        log('Starting benchmarks ...')
        results = Benchmark(network_graph)
        results.print_stats()
        save_object(result, picklename)
    return results


def is_blacklisted_file(name):
    BLACKLISTED_FILES = ['latencies', 'README', 'pikle', '.csv', 'DS_Store']
    for blacklist in BLACKLISTED_FILES:
        if blacklist in name:
            return True
    return False


def main(options):
    topologies = [options['folder']]
    objects = []
    for topo in topologies:
        if os.path.isdir(topo):
            # expand the dir and look for weighted rocketfuel topologies
            for name in os.listdir(topo):
                if not is_blacklisted_file(name):
                    topologies.append(os.path.join(topo, name))
        else:
            objects.append((topo, handle_path(topo, options['reload'], options['update'])))
    if options['exp']:
        perform_evaluation(objects)
    if options['export_paths']:
        export_path_stretch(objects)
    cleanup()


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print 'Usage: %s [-reload] [-exp] path -- path to a folder containing weighted rocketfuel topologies,' \
              ' possibly in subfolders.\n' \
              'If -update is specified, it will update the laoded object\n'\
              'If -paths is specified, it will export the paths along the pickled objects\n'\
              'If -random is specified, it will select the flows as random simple paths\n'\
              'If -exp is specified, it will perform some evaluation on the collected data.\n' \
              'If -reload is specified, it will attempt to reload any previously computed set of paths.' % sys.argv[0]
        sys.exit(-1)
    options = {
        'reload': '-reload' in sys.argv,
        'exp': '-exp' in sys.argv,
        'folder': sys.argv[len(sys.argv) - 1],
        'export_paths': '-paths' in sys.argv,
        'update': '-update' in sys.argv
    }
    flow.RANDOM_PATH_SELECTION = '-random' in sys.argv
    # in case we want to stop the benchmark ...
    signal.signal(signal.SIGINT, cleanup)
    print 'Logging to %s' % LOG_FILE
    logfile = open(LOG_FILE, 'a')
    start_time = datetime.datetime.now()
    log('===== Session start: %s' % str(start_time))
    main(options)
