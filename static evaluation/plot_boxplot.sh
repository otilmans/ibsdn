#!/bin/sh

# check arguments
function printUsage() {
        echo "Usage: $0 <results-file>"
        exit 1
}

[[ $# -eq 1 ]] || printUsage

# analyze input and options
resfile=$1

# graph with R
RSCRIPT=$(mktemp)
cat << EOF >  $RSCRIPT
        # Helper functions:
        #  1) compute the quantiles of a dataframe over a given field
        #f <- function(df, p=c(0, 0.25, 0.5, 0.75, 1))
        #        cbind( df[1,1:2], df.quantile(df[,"AS"], p))

        #  2) compute quantiles as a dataframe
        #data.quantile <- function(x, p)
        #        as.data.frame( t(quantile(x,p)) )

        # read the data and get rid of useless columns
        data = read.table(      "$resfile", 
                                col.names=c("xdim","ydim"))


        # plot the boxplots of "time" grouping by "phase"
        postscript(     "boxplot.eps", 
                        horizontal=FALSE, onefile=FALSE, height=6)
        par(mar=c(5,5,1,2))
        par(cex.axis=1.2, cex.lab=1.2, cex=1.2)
        boxplot(ydim ~ xdim, data=data, range=0, outline=FALSE,
				#log="y",
                #names=c("optimize","disseminate","rank","check"),
                xlab="Topology", ylab="Rule updates per single link failure")
EOF

R --no-save < $RSCRIPT && rm $RSCRIPT
