#!/usr/bin/env python
from oslo.config import cfg
from ryu import log
from ryu.controller.handler import get_dependent_services
from ryu.lib import hub
from ryu.base.app_manager import AppManager, RyuApp
from controller.controller import Controller
from compiler.network.net import load

CONF = cfg.CONF
CONF.register_cli_opt(
    cfg.StrOpt('input-network', required=True,
               help='A network topology produced by gen_network.')
)


def main():
    CONF(project='hybridnet', version='0.1')
    CONF.network = load(CONF.input_network)
    modules = [Controller]

    log.init_log()

    app_mgr = AppManager.get_instance()
    services = []
    for classname in modules:
        app_mgr.applications_cls[classname.__name__] = classname

        for key, context_cls in classname.context_iteritems():
            cls = app_mgr.contexts_cls.setdefault(key, context_cls)
            assert cls == context_cls

            if issubclass(context_cls, RyuApp):
                services.extend(get_dependent_services(context_cls))

        services.extend(get_dependent_services(classname))
        if services:
            app_mgr.load_apps(services)

    contexts = app_mgr.create_contexts()
    services = []
    services.extend(app_mgr.instantiate_apps(**contexts))

    try:
        hub.joinall(services)
    finally:
        app_mgr.close()


if __name__ == "__main__":
    main()