from ryu.lib import hub
from functools import wraps


"""The dictionnary of call count per key"""
callstats = {}


def count_calls(f):
    """Decorator to track the number of calls to the underlying function"""
    key = f.__name__

    @wraps(f)
    def inner(*args, **kwargs):
        callstats[key] += 1
        return f(*args, **kwargs)

    if key not in callstats:
        callstats[key] = 0
    return inner


def wait(event, duration):
    """Causes event to wait duration seconds"""
    event.clear()
    event.wait(duration)
