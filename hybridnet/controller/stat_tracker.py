import cPickle as pickle
from packetparser import PacketParser


class Tracker(object):
    def __init__(self, failure_src, flows, rate):
        self.failure_src = failure_src
        self.round = 0
        self.failure_round = 0
        self.probe_log = {}
        self.flows = flows
        self.probe_rate = rate
        self.reaction_time = 0
        self.affected_flows = 0
        self.flowmods = 0

    def new_round(self):
        self.round += 1

    def register_failure(self):
        self.failure_round = self.round

    def log_probe(self, cookie, probe):
        if not cookie in self.probe_log:
            self.probe_log[cookie] = [-1 for i in range(self.round - 1)]
        self.probe_log[cookie].append(probe)

    def log_stats(self, reaction_time, affected_flows, flowmods):
        self.reaction_time = reaction_time
        self.affected_flows = affected_flows
        self.flowmods = flowmods

    def save(self, name):
        with open('%s_%s.stats' % (name, self.failure_src), 'w') as f:
            pickle.dump(self, f)

    @staticmethod
    def load(name):
        with open(name, 'r') as f:
            return pickle.load(f)

    def probe_loss_ratio(self, parser=None):
        sent_probes = 0.0
        received_probes = 0.0
        if not parser:
            parser = PacketParser('rendered/localhost/netkit/capture')
        for cookie in self.probe_log:
            for probe in self.probe_log[cookie]:
                if probe != -1:
                    sent_probes += 1
                    if probe in parser.packets:
                        trace = parser.packets[probe]
                        if trace.has_trace(self.failure_src, self.flows[cookie]):
                            received_probes += 1.0
        return 100.0 - received_probes / sent_probes * 100.0

    def export(self, name, parser=None):
        lines = []
        if not parser:
             parser = PacketParser('rendered/localhost/netkit/capture')
        for cookie in self.probe_log:
            line = '%s;' % cookie
            for probe in self.probe_log[cookie]:
                if probe != -1:
                    line += '%s;' % probe
                    if probe in parser.packets:
                        trace = parser.packets[probe]
                        if trace.has_trace(self.failure_src, self.flows[cookie]):
                            line += '1;'  # received
                        else:
                            line += '0;'  # lost
                else:
                    line += '-1;1;'  # not appearing in logs !
            else:
                line += '-1;1;'
            lines.append(line)
        with open('%s_f%s_rate%s_fround%s.csv'
                          % (name, self.failure_src, self.probe_rate, self.failure_round), 'w') as f:
            for l in lines:
                f.write('%s\n' % l)
            f.close()



