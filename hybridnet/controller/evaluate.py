from oslo.config import cfg
import time
from paramiko import SSHClient, AutoAddPolicy
from ryu.base.app_manager import AppManager
from ryu.lib import hub
from compiler import CAPTURE_FOLDER
import signal
from ryu.ofproto.ether import ETH_TYPE_IP
from ryu.ofproto.inet import IPPROTO_UDP
import sys
import util
from flow import all_network_pairedflows, Flow, all_network_pairedflows_restricted
from probe import ProbePacket
from static_rules import RECOVERY_DSCP_CLASS, action_update_ethernet, install_groups, \
    log_probe_packet, send_back_lldp, drop_ipv6, normal_forward_recovery, \
    normal_forward_fallback, normal_forward_arp
import logging
from contextlib import closing
from stat_tracker import Tracker

logging.getLogger("paramiko").setLevel(logging.ERROR)

PROBE_DELAY = .05
COOKIE_MASK = 0xFFFFFFFF
PCAP_FILTER = "'udp src port 42'"  # "'not ip proto 89'"
NETKIT_USER = 'root'
NETKIT_PASSWORD = 'root'
FLOW_FACTOR = 100

CONF = cfg.CONF


def get_flow_ips(flow):
    return (i.ip for i in get_flow_bounding_interfaces(flow))


def get_flow_bounding_interfaces(flow):
    src_node = CONF.network.node(flow[0])
    first_hop_id = flow[1]
    last_hop_id = flow[len(flow) - 2]
    dst_node = CONF.network.node(flow[len(flow) - 1])
    return src_node.interface_for_dst(first_hop_id), dst_node.interface_for_dst(last_hop_id)


def get_shell(node):
    client = SSHClient()
    client.set_missing_host_key_policy(AutoAddPolicy())
    client.connect(str(node.tap_ip), username=NETKIT_USER, password=NETKIT_PASSWORD,
                   look_for_keys=False, allow_agent=False)
    return client


class NetworkEvaluation(object):
    def __init__(self, controller):
        self.controller = controller
        self.logger = controller.logger
        self.capture_instance = {}
        for node in CONF.network.nodes():
            for itf in node.interfaces_iter():
                self.capture_instance[(node.id, itf.id)] = 0
        signal.signal(signal.SIGINT, self.stop)
        self.sleep_event = hub.Event()
        self.probe_event = hub.Event()
        self.keep_probing = False
        self.probe_flows = []
        self.sent_probes = {}
        self.stat_tracker = None
        self.finished_probing = hub.Event()
        controller.spawn_thread(self.sleep, self.setup)
        controller.spawn_thread(self.probe_thread)

    def setup(self):
        self.extract_network_properties()
        self.setup_rules()
        self.install_flows()
        self.benchmark()

    def extract_network_properties(self):
        self.logger.info('Registering ethernet addresses')
        for node in CONF.network.nodes():
            for itf in node.interfaces_iter():
                itf.hwaddr = node.dp.ports[itf.of_portno].hw_addr

    def setup_rules(self):
        self.logger.info('Installing probing rules')
        for node in CONF.network.nodes():
            log_probe_packet(node)

    def install_flows(self):
        self.logger.info('Computing all paired flows')
        if CONF.network.node('source') and CONF.network.node('destination'):
            pairs = all_network_pairedflows_restricted(CONF.network.graph, 'source', 'destination')
        else:
            pairs = all_network_pairedflows(CONF.network.graph)
        flows = []
        for p in pairs.values():
            flows.extend(p.all_flows)
        self.logger.info('Evaluating the impact of failures on all disjoint'
                         ' flows in the network at once (%s)' % len(flows))
        for flow in flows:
            for i in range(FLOW_FACTOR):
                self.install_flow(Flow(CONF.network.graph, flow.flow))

    def install_static_rules(self, dp):
        send_back_lldp(dp)
        drop_ipv6(dp)

    def install_flow(self, flow):
        cookie = CONF.network.add_flow(flow)
        src_node = CONF.network.node(flow[0])
        src_ip, dst_ip = get_flow_ips(flow)
        flow.match = src_node.dp.ofproto_parser.OFPMatch(
            eth_type=ETH_TYPE_IP,
            ipv4_src=src_ip,
            ipv4_dst=dst_ip,
            ip_proto=IPPROTO_UDP,
            udp_dst=cookie
        )
        # no flow entry on the last node of the flow
        for index in range(0, len(flow) - 1):
            node = CONF.network.node(flow[index])
            self.install_flow_element(node, flow[index + 1], cookie, flow.match)

    def install_flow_element(self, node, next_id, cookie, match):
        parser = node.dp.ofproto_parser
        inst = [parser.OFPInstructionActions(
            node.dp.ofproto.OFPIT_APPLY_ACTIONS, self.action_output(node, next_id))]
        req = parser.OFPFlowMod(datapath=node.dp, cookie=cookie,
                                cookie_mask=COOKIE_MASK, match=match, instructions=inst)
        node.dp.send_msg(req)

    def sleep(self, func=None, clear_event=True):
        if clear_event:
            self.sleep_event.clear()
        self.sleep_event.wait()
        if func:
            func()

    def awake(self):
        self.sleep_event.set()

    def stop(self, *args, **kwargs):
        self.stop_probing()
        AppManager.get_instance().close()
        ProbePacket.save_id()
        sys.exit(0)

    def benchmark(self):
        benchmark_start = time.time()
        self.logger.info('Starting to bring links down')
        self.link_downs()
        benchmark_end = time.time()
        self.logger.info('Elapsed time: %s' % ((benchmark_end - benchmark_start) * 1000))
        self.stop(0)

    def link_downs(self):
        ev = hub.Event()
        for src, dst in CONF.network.links():

            self.logger.info('Testing link (%s, %s)' % (src, dst))
            if not CONF.network.can_remove_link(src, dst):
                # Don't remove links causing a network partition
                self.logger.info('Removing it causes a network partition!')
                continue
            self.probe_flows = []
            for c, f in CONF.network.flows_with_link(src, dst, directed=True):
                source = self.get_probe_sending_point(f, (src, dst))
                if f.link_source(src, dst) == src and not f.flow[len(f.flow) - 1] == dst and source:
                    self.probe_flows.append((c, f, source))
            if len(self.probe_flows) == 0:
                print 'no flows'
                continue
            self.stat_tracker = Tracker(src,
                                        {c: f.flow for c, f, _ in self.probe_flows},
                                        PROBE_DELAY)
            itf = CONF.network.node(src).interface_for_dst(dst)
            self.prekill_interface(itf)
            self.start_probing()
            util.wait(ev, 5)
            self.kill_interface(itf)
            self.stat_tracker.register_failure()
            util.wait(ev, 4)
            self.stop_probing()
            # Need to manually clear the sleep event before bringing the interface up
            self.sleep_event.clear()
            self.restore_interface(itf)
            # Wait until the link is back up
            self.sleep(clear_event=False)
            self.restore_listening(itf)
            self.postrestore_interface(itf)
            self.stat_tracker.save('%sf_%s_%s' % (FLOW_FACTOR, CONF.mode, CONF.input_network))
            util.wait(ev, 2)

    def get_probe_sending_point(self, flow, failure):
        path = flow.flow
        src = CONF.network.node(path[0])
        for neighbor in CONF.network.neighbors(src.id):
            if neighbor.id not in path:
                return neighbor.id
        return None

    def print_link_stats(self, src, dst):
        self.logger.info('Probe loss: %s' % self.stat_tracker.probe_loss_ratio())

    @util.count_calls
    def kill_interface(self, interface):
        with closing(get_shell(interface.node)) as shell:
            shell.exec_command('ifconfig %s down' % interface.id)
        self.capture_instance[(interface.node.id, interface.id)] += 1

    def restore_interface(self, interface):
        with closing(get_shell(interface.node)) as shell:
            shell.exec_command('ifconfig %s up' % interface.id)

    def restore_listening(self, interface):
        with closing(get_shell(interface.node)) as shell:
            shell.exec_command(
                'nohup tcpdump -U -i %s -w /hostlab/%s/%s-%s-%s-%s.pcap %s > /dev/null 2> /dev/null < /dev/null &'
                % (interface.id, CAPTURE_FOLDER, interface.node.id, interface.id, interface.hwaddr,
                   self.capture_instance[(interface.node.id, interface.id)], PCAP_FILTER))

    def probe_flow(self, cookie, flow, sending_point):
        src_itf, dst_itf = get_flow_bounding_interfaces(flow)
        send_node = CONF.network.node(sending_point)
        send_itf = send_node.interface_for_dst(src_itf.node.id)
        probe = ProbePacket(src_mac=send_itf.hwaddr, dst_mac=send_itf.bound_to[0].hwaddr,
                            dest_ip=dst_itf.ip, src_ip=src_itf.ip, port=cookie)
        if self.stat_tracker.failure_src == 'r1':
            backup_next_hop = 'r7'
        elif self.stat_tracker.failure_src == 'r3':
            backup_next_hop = 'r1'
        elif self.stat_tracker.failure_src == 'r4':
            backup_next_hop = 'r3'
        elif self.stat_tracker.failure_src == 'r5':
            backup_next_hop = 'r4'
        backup_itf = CONF.network.node(self.stat_tracker.failure_src).interface_for_dst(backup_next_hop)
        backup_probe = ProbePacket(src_mac=backup_itf.hwaddr, dst_mac=backup_itf.bound_to[0].hwaddr,
                                   dest_ip=dst_itf.ip, src_ip=src_itf.ip, port=cookie, tos=RECOVERY_DSCP_CLASS<<2)
        backup_probe.output(backup_itf.node.dp, 0, backup_itf.of_portno)
        self.stat_tracker.log_probe(cookie, probe.id)
        probe.output(send_node.dp, 0, send_itf.of_portno)
        self.logger.debug('Sent probe %s from %s[%s] to %s | %s'
                          % (probe.id, src_itf.node.id, src_itf.id, dst_itf.node.id, flow))

    def probe(self):
        event = hub.Event()
        while self.keep_probing:
            for cookie, flow, sending_point in self.probe_flows:
                self.probe_flow(cookie, flow, sending_point)
            event.clear()
            event.wait(PROBE_DELAY)
            self.stat_tracker.new_round()

    def probe_thread(self):
        while self.controller.is_active:
            self.probe_event.wait()
            self.probe()
            self.finished_probing.set()
            self.probe_event.clear()

    def start_probing(self):
        self.keep_probing = True
        self.finished_probing.clear()
        self.probe_event.set()

    def stop_probing(self):
        self.keep_probing = False
        self.finished_probing.wait()

    def link_down(self, dpid, port_no):
        pass

    def link_up(self, dpid, port_no):
        self.awake()

    def action_output(self, node, towards):
        parser = node.dp.ofproto_parser
        out_itf = node.interface_for_dst(towards)
        actions = action_update_ethernet(node, out_itf.of_portno)
        actions.append(parser.OFPActionDecNwTtl())
        actions.append(parser.OFPActionOutput(out_itf.of_portno))
        return actions

    def prekill_interface(self, interface):
        pass

    def postrestore_interface(self, interface):
        pass


class Reactive(NetworkEvaluation):
    def __init__(self, *args, **kwargs):
        super(Reactive, self).__init__(*args, **kwargs)

    @util.count_calls
    def link_down(self, dpid, port_no):
        #ev = hub.Event()
        #util.wait(ev, .01)
        start = time.time()
        src = CONF.network.dpid(dpid)
        dst = src.port(port_no).bound_to[0].node
        repaired = 0
        mods = 0
        self.logger.debug('Repairing affected flows')
        for cookie, flow in CONF.network.flows_with_link(src.id, dst.id):
            repaired += 1
            mods += self.repair_flow(flow, cookie, flow.link_source(src.id, dst.id))
        end = time.time()
        self.logger.info('Reacted in %s | affected %s flows | %s flowmods'
                         % ((end - start) * 1000, repaired, mods))
        self.stat_tracker.log_stats((end - start) * 1000, repaired, mods)

    @util.count_calls
    def repair_flow(self, flow, cookie, failed_src):
        backup_flow = flow.backup_for(failed_src)
        self.logger.debug('Established backup flow: %s' % backup_flow)
        backup_flow.cookie = cookie
        CONF.network.add_backup_flow(cookie, backup_flow)
        flow_mods = backup_flow.flow_mods()  # backup_flow.backup_flow  #
        # because we need it to route the last flow mod entry
        flow_mods.append(backup_flow.convergence_host())
        mods = 0
        for index in range(0, len(flow_mods) - 1):
            node = CONF.network.node(flow_mods[index])
            parser = node.dp.ofproto_parser
            ofp = node.dp.ofproto
            actions = self.action_output(node, flow_mods[index + 1])
            actions.insert(0, parser.OFPActionSetField(ip_dscp=RECOVERY_DSCP_CLASS))
            inst = [parser.OFPInstructionActions(ofp.OFPIT_APPLY_ACTIONS, actions)]
            req = parser.OFPFlowMod(datapath=node.dp, cookie=cookie,
                                    command=ofp.OFPFC_MODIFY if index == 0 else ofp.OFPFC_ADD,
                                    cookie_mask=COOKIE_MASK, match=flow.match, instructions=inst)
            self.backup_flowmod(node.dp, req)
            mods += 1
        return mods

    @util.count_calls
    def backup_flowmod(self, dp, req):
        dp.send_msg(req)

    def link_up(self, dpid, port_no):
        CONF.network.clear_backup_flows()
        super(Reactive, self).link_up(dpid, port_no)

    def get_probe_sending_point(self, flow, failure):
        path = flow.flow
        src = CONF.network.node(path[0])
        failure_source = flow.link_source(*failure)
        backup = flow.backup_for(failure_source).backup_flow
        for neighbor in CONF.network.neighbors(src.id):
            if neighbor.id not in path and neighbor.id not in backup:
                return neighbor.id
        return None


class Hybrid(NetworkEvaluation):
    COST = 200
    PATTERN = 'interface "%s" {\n                        cost %s;'

    def __init__(self, *args, **kwargs):
        super(Hybrid, self).__init__(*args, **kwargs)

    def setup_rules(self):
        super(Hybrid, self).setup_rules()
        self.logger.info('Installing groups')
        for node in CONF.network.nodes():
            install_groups(node)

    def install_static_rules(self, dp):
        super(Hybrid, self).install_static_rules(dp)
        normal_forward_arp(dp)
        normal_forward_fallback(dp)
        normal_forward_recovery(dp)

    def action_output(self, node, towards):
        parser = node.dp.ofproto_parser
        out_itf = node.interface_for_dst(towards)
        return [parser.OFPActionGroup(out_itf.of_portno)]

    def prekill_interface(self, interface):
        ev = hub.Event()
        Hybrid.set_disabled(interface)
        Hybrid.set_disabled(interface.bound_to[0])
        with closing(get_shell(interface.bound_to[0].node)) as shell:
            shell.exec_command('cp /hostlab/%s/etc/bird.conf /etc/bird.conf' % interface.bound_to[0].node.id)
            shell.exec_command('birdc configure')
            shell.exec_command('birdc configure confirm')
        with closing(get_shell(interface.node)) as shell:
            shell.exec_command('cp /hostlab/%s/etc/bird.conf /etc/bird.conf' % interface.node.id)
            shell.exec_command('birdc configure')
            shell.exec_command('birdc configure confirm')
            util.wait(ev, 5)

    def postrestore_interface(self, interface):
        ev = hub.Event()
        Hybrid.set_disabled(interface, False)
        Hybrid.set_disabled(interface.bound_to[0], False)
        with closing(get_shell(interface.bound_to[0].node)) as shell:
            shell.exec_command('cp /hostlab/%s/etc/bird.conf /etc/bird.conf' % interface.bound_to[0].node.id)
            shell.exec_command('birdc configure')
            shell.exec_command('birdc configure confirm')
        with closing(get_shell(interface.node)) as shell:
            shell.exec_command('cp /hostlab/%s/etc/bird.conf /etc/bird.conf' % interface.node.id)
            shell.exec_command('birdc configure')
            shell.exec_command('birdc configure confirm')
            util.wait(ev, 5)


    @staticmethod
    def set_disabled(interface, swap=True):
        fname = 'rendered/localhost/netkit/%s/etc/bird.conf' % interface.node.id
        with closing(open(fname, 'r')) as f:
            content = f.read()
        content = content.replace(Hybrid.PATTERN % ('1' if swap else '100', interface.id),
                                  Hybrid.PATTERN % ('-' if swap else '100', interface.id))
        with closing(open(fname, 'w')) as f:
            f.write(content)


class ProActive(NetworkEvaluation):
    def __init__(self, *args, **kwargs):
        super(ProActive, self).__init__(*args, **kwargs)
        self.logger.error('Proactive approach not implemented!')