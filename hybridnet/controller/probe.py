from ryu.lib.packet.ethernet import ethernet
from ryu.lib.packet.ipv4 import ipv4
from ryu.lib.packet.packet import Packet
from ryu.lib.packet.udp import udp
from ryu.ofproto.inet import IPPROTO_UDP
import os

class ProbePacket(object):
    class UnknownFormat(Exception):
        message = '%(msg)s'

    NEXT_ID = 1
    PROBE_SOURCE_PORT = 42
    PCAP_FILTER = "'udp src port %s'" % PROBE_SOURCE_PORT

    def __init__(self, src_mac, dst_mac, dest_ip='0.0.0.0', src_ip='0.0.0.0', port=12345, tos=0):
        self.id = ProbePacket.NEXT_ID
        self.dest_ip = dest_ip
        self.port = port
        self.src_ip = src_ip
        ProbePacket.NEXT_ID += 1
        self.src_mac = src_mac
        self.dst_mac = dst_mac
        self.tos = tos

    def __repr__(self):
        return '<ProbePacket id:%s>' % self.__str__()

    def __str__(self):
        return str(self.id)

    def serialize(self):
        pkt = Packet()
        pkt / ethernet(dst=self.dst_mac, src=self.src_mac)
        pkt / ipv4(dst=self.dest_ip, src=self.src_ip, proto=IPPROTO_UDP, tos=self.tos)
        pkt / udp(src_port=ProbePacket.PROBE_SOURCE_PORT, dst_port=self.port)
        pkt / self
        pkt.serialize()
        return pkt.data

    @staticmethod
    def parse_probe(data):
        pkt = Packet(data=data)
        i = iter(pkt)
        eth_pkt = i.next()
        if type(eth_pkt) != ethernet:
            raise ProbePacket.UnknownFormat("Not an ethernet frame")
        ip_pkt = i.next()
        if type(ip_pkt) != ipv4:
            raise ProbePacket.UnknownFormat("Not an ipv4 packet")
        ttl = ip_pkt.ttl
        udp_pkt = i.next()
        if type(udp_pkt) != udp:
            raise ProbePacket.UnknownFormat("Not an UDP packet")
        if udp_pkt.src_port != ProbePacket.PROBE_SOURCE_PORT:
            raise Exception("Not a probe packet")
        return i.next(), ttl, ip_pkt.tos

    def output(self, dp, inport, outport):
        dp.send_msg(dp.ofproto_parser.OFPBarrierRequest(dp))
        action = [dp.ofproto_parser.OFPActionOutput(outport)]
        out = dp.ofproto_parser.OFPPacketOut(
            datapath=dp, in_port=inport,
            buffer_id=dp.ofproto.OFP_NO_BUFFER, actions=action,
            data=self.serialize())
        dp.send_msg(out)

    @staticmethod
    def save_id():
        with open('PROBE_ID', 'w') as f:
            f.write(str(ProbePacket.NEXT_ID))
            f.close()

    @staticmethod
    def load_id():
        if os.path.exists('PROBE_ID'):
            with open('PROBE_ID', 'r') as f:
                ProbePacket.NEXT_ID = int(f.readline())
                print 'resumed probe id: %s' % ProbePacket.NEXT_ID
