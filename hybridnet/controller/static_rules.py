from ryu.ofproto.ether import (ETH_TYPE_LLDP, ETH_TYPE_ARP,
                               ETH_TYPE_IP, ETH_TYPE_IPV6)
from ryu.lib.packet.lldp import LLDP_MAC_NEAREST_BRIDGE
from ryu.ofproto.inet import IPPROTO_TCP, IPPROTO_UDP
from probe import ProbePacket

RECOVERY_DSCP_CLASS = 8


def forward(dp, match, actions, prio=0xFFFF):
    inst = [dp.ofproto_parser.OFPInstructionActions(
        dp.ofproto.OFPIT_APPLY_ACTIONS, actions)]
    mod = dp.ofproto_parser.OFPFlowMod(datapath=dp, match=match,
                                       idle_timeout=0, hard_timeout=0,
                                       instructions=inst,
                                       priority=prio)
    dp.send_msg(mod)


def normal_forward(dp, match, actions=[], prio=0xFFFF):
    a = actions + [dp.ofproto_parser.OFPActionOutput(dp.ofproto.OFPP_NORMAL)]
    forward(dp, match, a, prio)


def controller_forward(dp, match, prio=0xFFFF):
    actions = [dp.ofproto_parser.OFPActionOutput(
        dp.ofproto.OFPP_CONTROLLER,
        dp.ofproto.OFPCML_NO_BUFFER)]
    forward(dp, match, actions, prio)


def send_back_lldp(dp):
    match = dp.ofproto_parser.OFPMatch(
        eth_type=ETH_TYPE_LLDP,
        eth_dst=LLDP_MAC_NEAREST_BRIDGE)
    controller_forward(dp, match)


def normal_forward_arp(dp):
    match = dp.ofproto_parser.OFPMatch(
        eth_type=ETH_TYPE_ARP
    )
    normal_forward(dp, match, prio=0)


def normal_forward_fallback(dp):
    match = dp.ofproto_parser.OFPMatch(
        eth_type=ETH_TYPE_IP
    )
    a = [dp.ofproto_parser.OFPActionSetField(ip_dscp=RECOVERY_DSCP_CLASS)]
    normal_forward(dp, match, actions=a, prio=0)


def normal_forward_recovery(dp):
    match = dp.ofproto_parser.OFPMatch(
        eth_type=ETH_TYPE_IP,
        ip_dscp=RECOVERY_DSCP_CLASS
    )
    normal_forward(dp, match)


def drop_ipv6(dp):
    match = dp.ofproto_parser.OFPMatch(
        eth_type=ETH_TYPE_IPV6,
    )
    inst = [dp.ofproto_parser.OFPInstructionActions(
        dp.ofproto.OFPIT_APPLY_ACTIONS, [])]
    mod = dp.ofproto_parser.OFPFlowMod(datapath=dp, match=match,
                                       idle_timeout=0, hard_timeout=0,
                                       instructions=inst,
                                       priority=0)
    dp.send_msg(mod)


def action_update_ethernet(node, port):
    ofp = node.dp.ofproto_parser
    src_port = node.port(port)
    src = src_port.hwaddr
    dst = src_port.bound_to[0].hwaddr
    update_src = ofp.OFPActionSetField(eth_src=src)
    update_dst = ofp.OFPActionSetField(eth_dst=dst)
    return [update_src, update_dst]


def install_groups(node):
    dp = node.dp
    ofp = dp.ofproto
    ofp_parser = dp.ofproto_parser
    normal_action = [ofp_parser.OFPActionSetField(ip_dscp=RECOVERY_DSCP_CLASS),
                     ofp_parser.OFPActionOutput(ofp.OFPP_NORMAL)]
    normal_bucket = ofp_parser.OFPBucket(0, ofp.OFPP_LOCAL, ofp.OFPG_ANY,
                                         normal_action)
    for port in dp.ports.values():
        nr = port.port_no
        if nr < node.dp.ofproto.OFPP_MAX:
            actions = action_update_ethernet(node, nr)
            actions.insert(0, ofp_parser.OFPActionDecNwTtl())
            actions.append(ofp_parser.OFPActionOutput(nr))
            buckets = [ofp_parser.OFPBucket(100, nr, ofp.OFPG_ANY, actions),
                       normal_bucket]
            req = ofp_parser.OFPGroupMod(node.dp, ofp.OFPGC_ADD,
                                         ofp.OFPGT_FF, nr, buckets)
            dp.send_msg(req)


def log_probe_packet(node):
    for itf in node.interfaces_iter():
        ip = itf.ip
        dp = node.dp
        match = dp.ofproto_parser.OFPMatch(
            eth_type=ETH_TYPE_IP,
            ip_proto=IPPROTO_UDP,
            ipv4_dst=ip,
            udp_src=ProbePacket.PROBE_SOURCE_PORT
        )
        controller_forward(dp, match)
        match_recovered = dp.ofproto_parser.OFPMatch(
            eth_type=ETH_TYPE_IP,
            ip_proto=IPPROTO_UDP,
            ip_dscp=RECOVERY_DSCP_CLASS,
            ipv4_dst=ip,
            udp_src=ProbePacket.PROBE_SOURCE_PORT
        )
        controller_forward(dp, match_recovered)


def cleanup_switch(dp):
    parser = dp.ofproto_parser
    ofp = dp.ofproto
    req = parser.OFPFlowMod(datapath=dp, command=ofp.OFPFC_DELETE, table_id=ofp.OFPTT_ALL)
    dp.send_msg(req)
    req = parser.OFPGroupMod(datapath=dp, command=ofp.OFPGC_DELETE, group_id=ofp.OFPG_ALL)
    dp.send_msg(req)
