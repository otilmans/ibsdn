from oslo.config import cfg
from ryu.base import app_manager
from ryu.controller import ofp_event
from ryu.controller.handler import set_ev_cls, MAIN_DISPATCHER, DEAD_DISPATCHER
from ryu.lib import hub
from ryu.topology.switches import LLDPPacket
from ryu import utils
from evaluate import Hybrid, Reactive, ProActive
from probe import ProbePacket
from static_rules import cleanup_switch
import util

DEFAULT_TTL = 255

CONF = cfg.CONF
CONF.register_cli_opts([
    cfg.StrOpt('mode', required=False, default='hybrid',
               help='Mode of operation: reactive/proactive/[hybrid]')
])


class Controller(app_manager.RyuApp):
    def __init__(self, *args, **kwargs):
        ProbePacket.load_id()
        util.DELAY = CONF.delay
        self.pending_links = {}
        super(Controller, self).__init__(*args, **kwargs)
        self.dissectors = (self.dissect_lldp, self.dissect_probe)
        self.logger.info('## [%s] mode ##' % CONF.mode.upper())
        if CONF.mode == 'reactive':
            self.evaluation = Reactive(self)
        elif CONF.mode == 'proactive':
            self.evaluation = ProActive(self)
        else:
            self.evaluation = Hybrid(self)
        self.logger.info('Waiting for connections ...')

    def spawn_thread(self, proc, *args, **kwargs):
        self.threads.append(hub.spawn(proc, *args, **kwargs))

    @set_ev_cls(ofp_event.EventOFPStateChange, [MAIN_DISPATCHER, DEAD_DISPATCHER])
    def switch_handler(self, ev):
        dp = ev.datapath
        dp_str = CONF.network.dpid(dp.id).id
        if ev.state == MAIN_DISPATCHER:
            self.logger.info('Switch %s connected' % dp_str)
            self.configure_dp(dp)
        elif ev.state == DEAD_DISPATCHER:
            self.logger.info('Switch %s disconnected' % dp_str)
            CONF.network.unregister_dp(dp)
            # TODO reactive stuff ?
        else:
            self.logger.error('Unknown OFPStateChange for %s' % dp_str)

    @set_ev_cls(ofp_event.EventOFPPacketIn, MAIN_DISPATCHER)
    def packet_in_handler(self, ev):
        msg = ev.msg
        dp = msg.datapath
        proto = dp.ofproto
        if msg.reason == proto.OFPR_NO_MATCH:
            pass
            #self.logger.debug('No match at %s for %s'
            #                  % (CONF.network.port_str(dp.id, msg.match['in_port']),
            #                     '%s...' % utils.hex_array(msg.data)[0:32]))
        elif msg.reason == proto.OFPR_ACTION:
            self.analyze_packet(msg, dp)
        elif msg.reason == proto.OFPR_INVALID_TTL:
            self.logger.debug("Invalid TTL at %s for %s"
                              % (CONF.network.port_str(dp.id, msg.match['in_port']),
                                 utils.hex_array(msg.data)[0:32]))

    @set_ev_cls(ofp_event.EventOFPPortStatus, MAIN_DISPATCHER)
    def link_handler(self, ev):
        msg = ev.msg
        reason = msg.reason
        dp = msg.datapath
        ofpport = msg.desc
        ofproto = dp.ofproto
        port_str = CONF.network.port_str(dp.id, ofpport.port_no)
        if reason == ofproto.OFPPR_ADD:
            self.logger.info('Ignoring OFPPR_ADD for %s' % port_str)
        elif reason == ofproto.OFPPR_DELETE:
            self.logger.info('Ignoring OFPPR_DELETE for %s' % port_str)
        elif reason == ofproto.OFPPR_MODIFY:
            self.modify_port_state(ofpport, dp, port_str)
        else:
            self.logger.error('Unknown OFPPortStatus for %s' % port_str)

    def modify_port_state(self, ofpport, dp, port_str):
        ofproto = dp.ofproto
        is_live = (ofpport.state & ofproto.OFPPS_LIVE) > 0 \
            or not ((ofpport.state & ofproto.OFPPS_LINK_DOWN) > 0
                    or (ofpport.config & ofproto.OFPPC_PORT_DOWN) > 0)
        if CONF.network.update_port_liveness(dpid=dp.id,
                                             port_no=ofpport.port_no,
                                             is_live=is_live):
            if is_live:
                self.logger.debug('%s is now up' % port_str)
                self.evaluation.link_up(dp.id, ofpport.port_no)
            else:
                self.logger.info('%s is now down' % port_str)
                self.spawn_thread(self.evaluation.link_down, dp.id, ofpport.port_no)

    def configure_dp(self, dp):
        # ensures the switch ahs no groups/flows set
        cleanup_switch(dp)
        self.evaluation.install_static_rules(dp)
        if CONF.network.register_dp(dp):
            # If we have all datapath pointers, let's discover the links
            # need another thread to not block the async switch->controller links
            self.spawn_thread(self.explore_links)

    def explore_links(self):
        # TODO should only send one packet per link (instead of 2 ATM)
        # is it even possible ?
        self.logger.info('Exploring links')
        for node in CONF.network.nodes():
            # ignore reserved ports i.e LOCAL
            for port in node.dp.ports.values():
                self.craft_lldp(port, node)

    def craft_lldp(self, port, node):
        port_no = port.port_no
        dp = node.dp
        if port_no < dp.ofproto.OFPP_MAX:
            pkt = LLDPPacket.lldp_packet(dp.id, port_no, port.hw_addr, DEFAULT_TTL)
            actions = [dp.ofproto_parser.OFPActionOutput(port_no)]
            out = dp.ofproto_parser.OFPPacketOut(
                datapath=dp, in_port=dp.ofproto.OFPP_CONTROLLER,
                buffer_id=dp.ofproto.OFP_NO_BUFFER, actions=actions,
                data=pkt)
            # ensures we fill the dict before sending the packet
            self.pending_links[(node.dpid, port_no)] = True
            dp.send_msg(out)

    def analyze_packet(self, msg, dp):
        for f in self.dissectors:
            # dissectors will return true if the packet is consumed
            if f(msg, dp):
                break

    def dissect_probe(self, msg, dp):
        try:
            id, _, tos = ProbePacket.parse_probe(msg.data)
            self.logger.debug('Received probe %s on %s [TOS:%s]' %
                              (id, CONF.network.id_for(dp.id), tos))
            return True
        except ProbePacket.UnknownFormat:
            return False

    def dissect_lldp(self, msg, dp):
        try:
            src_dpid, src_port_no = LLDPPacket.lldp_parse(msg.data)
            CONF.network.register_link(src_dpid, src_port_no, dp.id, msg.match['in_port'])
            del self.pending_links[(src_dpid, src_port_no)]
            # Check if some links still need to report
            if not self.pending_links:
                print CONF.network
                self.logger.info('All links have been explored')
                # And start the testing phase
                self.evaluation.awake()
            return True
        except LLDPPacket.LLDPUnknownFormat:
            return False