from contextlib import closing
import os
import dpkt
from ryu.lib.packet.ethernet import ethernet
from ryu.lib.packet.packet import Packet
from static_rules import RECOVERY_DSCP_CLASS
from probe import ProbePacket

PCAP_EXT = '.pcap'


class PacketTrace(object):
    def __init__(self):
        self.trace = {} # ttl: [ports]

    def log(self, ttl, port, tos):
        if ttl not in self.trace:
            self.trace[ttl] = []
        self.trace[ttl].append((port.split('-')[0], tos))

    def __repr__(self):
        return ' -> '.join([self.ttl_entry(e) for e in self.ordered_trace()])

    def ordered_trace(self):
        ttls = self.trace.keys()
        ttls.sort(reverse=True)
        return [item for ttl in ttls for item in self.trace[ttl]]

    def ttl_entry(self, e):
        return '%s|%s' % e

    def has_trace(self, failure, flow):
        trace = [hop for hop, _ in self.ordered_trace()]
        #print '%s --> %s' % (flow, trace)
        #return (failure in trace and trace.index(failure) < len(trace)-1) or \
        return       trace[len(trace) - 1] == flow[len(flow) - 1]


def destinated_to(pkt, hwaddr):
    p = Packet(data=pkt)
    eth = iter(p).next()
    if type(eth) != ethernet:
        return False
    return True if eth.dst == hwaddr else False


class PacketParser(object):
    def __init__(self, directory, index=0):
        self.directory = directory
        self.packets = {} # id: PacketTrace
        self.index = 0
        self.parse()

    def parse(self):
        files = os.listdir(self.directory)
        files.sort()
        for filename in files:
            if filename.endswith(PCAP_EXT):
                self.parse_file(filename)

    def parse_file(self, filename):
        path = os.path.join(self.directory, filename)
        with closing(open(path, 'r')) as f:
            try:
                pcap = dpkt.pcap.Reader(f)
                port = filename[:-len(PCAP_EXT)]
                hwaddr = port.split('-')[2]
                for _, pkt in pcap:
                    if destinated_to(pkt, hwaddr):
                        self.log_pkt(pkt, port)
            except dpkt.NeedData:
                print 'No packets in %s' % filename

    def log_pkt(self, pkt, port):
        try:
            id, ttl, tos = ProbePacket.parse_probe(pkt)
            id = int(id)
            if id not in self.packets:
                self.packets[id] = PacketTrace()
            self.packets[id].log(ttl, port, tos)
        except ProbePacket.UnknownFormat:
            pass

    def list_traces(self):
        ids = self.packets.keys()
        ids.sort(key=int)
        return '\n'.join(['Probe %s| %s' % (id, self.packets[id]) for id in ids if int(id) >= self.index])