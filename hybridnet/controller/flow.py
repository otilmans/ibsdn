RANDOM_PATH_SELECTION = False # Whether to use random path selection to choose flows
MAX_DETOUR = 1.5 # the maximal path length when choosing a path at random


def path_selection(network, src, dst):
    """Hook to setup the way flows are chosen in the network"""
    from networkx import shortest_path, all_simple_paths, has_path, NetworkXNoPath
    
    try:
        shortest = shortest_path(network, source=src, target=dst, weight='ospf_cost')
    except NetworkXNoPath:
        # network is disjoint
        return None
    if RANDOM_PATH_SELECTION:
        try:
            paths = all_simple_paths(network, source=src, target=dst, cutoff=MAX_DETOUR*len(shortest))
            return paths.next()
        except StopIteration:
            return None
    else:
        return shortest


def backup_path_selection(network, src, dst):
    """Hook to setup the way backup paths are chosen"""
    return path_selection(network, src, dst)


class Flow(object):
    """A class holding a flow in the network, and possibly its backup flows"""

    def __init__(self, network, flow):
        self.flow = flow  # a list of hosts
        self.network = network  # the network graph
        self.backups = {}  # failure source -> BackupFlow
        self.ibs_backups = {}
        self.match = None  # matching clause associated to this flow

    def contains_link(self, n1, n2):
        """Return wether this flow contains the undirected link n1-n2"""
        if len(self.flow) == 2:
            return (self.flow[0] == n1 and self.flow[1] == n2)\
                or (self.flow[0] == n2 and self.flow[1] == n2)
        for index in range(1, len(self.flow) - 1):
            elem = self.flow[index]
            prev = self.flow[index - 1]
            next = self.flow[index + 1]
            test = lambda x, y: \
                elem == x and (prev == y or next == y)
            if test(n1, n2) or test(n2, n1):
                return True
        return False

    def link_source(self, src, dst):
        """Return the source of the given link
        e.g. the first of the two hosts reached in the flow
        Assumes that the link is present !
        """
        for host in self.flow:
            if host == src:
                return src
            elif host == dst:
                return dst
        # should never be reached
        return None

    def compute_backups(self):
        """Compute the backup flows to protect this flow"""
        for i in range(0, len(self.flow) - 1):
            backup = self.backup_for_index(i)
            if backup:
                self.backups[(self.flow[i], self.flow[i + 1])] = backup
                self.ibs_backups[(self.flow[i], self.flow[i + 1])] = self.ibs_backup_for_index(i)

    def backup_for(self, failed_link_src):
        """Return the backup flow protecting this object if failed_link_src fails"""
        return self.backup_for_index(self.flow.index(failed_link_src))

    def backup_for_index(self, failed_index):
        """Return the backup flow protecting this object if
        the link between self.flow[failed_index] and self.flow[failed_index+1] fails"""
        edge_start = self.flow[failed_index]
        edge_end = self.flow[failed_index + 1]
        data = self.network.get_edge_data(edge_start, edge_end, {'ospf_cost': 1})
        # simulate a link failure
        self.network.remove_edge(edge_start, edge_end)
        new_flow = backup_path_selection(self.network, self.flow[0], self.flow[len(self.flow) - 1])
        # if no new flow then the failure results in a network partition
        # otherwise compute backup flow
        backup = BackupFlow(self, new_flow) if new_flow else None
        # reconstruct the network for the next failure
        self.network.add_edge(edge_start, edge_end, **data)
        return backup
        
    def ibs_backup_for_index(self, failed_index):
        """Return the effective backup in IBSDN if
        the link between self.flow[failed_index] and self.flow[failed_index+1] fails"""
        edge_start = self.flow[failed_index]
        edge_end = self.flow[failed_index + 1]
        data = self.network.get_edge_data(edge_start, edge_end, {'ospf_cost': 1})
        # simulate a link failure
        self.network.remove_edge(edge_start, edge_end)
        new_flow = backup_path_selection(self.network, edge_start, self.flow[len(self.flow) - 1])
        # if no new flow then the failure results in a network partition
        # otherwise compute backup flow
        if new_flow:
            backup = self.flow[0:failed_index]
            backup.extend(new_flow)
        else:
            backup = None
        # reconstruct the network for the next failure
        self.network.add_edge(edge_start, edge_end, **data)
        return backup

    # Reimplemented to ease up life
    def __len__(self):
        return len(self.flow)

    def __getitem__(self, index):
        return self.flow[index]

    def __iter__(self):
        return iter(self.flow)

    def __contains__(self, host):
        return host in self.flow

    def __repr__(self):
        return ', '.join(self.flow)


class BackupFlow(Flow):
    """A class representing a backup flow for a given flow"""

    def __init__(self, linked_flow, new_flow):
        super(BackupFlow, self).__init__(linked_flow.network, linked_flow.flow)
        self.match = linked_flow.match  # inherit the matching clause
        self.backup_flow = new_flow  # the backup flow
        self.divergence_point, self.convergence_point = BackupFlow.compute_flow_mods(self.flow, new_flow)
        self.cookie = None  # used by the controller to link flows together

    def flow_mods(self):
        """Return the routers that needs to receive a flow mod message
        to have this backup flow installed in the network
        
        It is important to note that the returned set of router is only valid
        if the matching clauses for the flows do not include matches on the input
        port. If that was the case, then we would also need to include one router
        after the convergence point"""
        # The flow mods to send are starting at divergence_point and convergence_point included at the very least,
        # the 'old' flow entries will timeout if no packet matches them
        return [self.backup_flow[i] for i in range(self.divergence_point,
                                                   (self.convergence_point + 1
                                                    if self.convergence_point < len(self.backup_flow) - 1
                                                    else self.convergence_point))]

    def __repr__(self):
        return '%s -> %s' % (', '.join(self.flow), ', '.join(self.backup_flow))

    @staticmethod
    def compute_flow_mods(oldflow, new_flow):
        """Compute the divergence point and the converge point of the backup flow
        If we have the flow r1-r2-r3-r4-r5 and the backup flow r1-r2-r6-r7-r4-r5,
        we define the divergence point as r6 in the backup flow and r3 in the original flow
        we define the convergence point as r4 in both flow
        from these two point we can compute the minimal set of router that need to be reconfigured
        along the backup flow as 
        [divergence point -1, convergence point [ If the flows do not match against the input port
        [divergence point -1, convergence point ] Otherwise
        """
        # infer the changes that needs to be done in the network to install it
        # Skip to the first point of divergence
        # len(self.flow) is necessarily the lower bound as it was the shortest_path of a network with more links
        divergence_point = 0
        while divergence_point < len(oldflow) \
                and oldflow[divergence_point] == new_flow[divergence_point]:
            divergence_point += 1
        # Same reasoning but from the end of the backup flow
        # However we need to deal with two indexes as the new_flow might be longer
        convergence_point = len(new_flow) - 1
        comparison_point = len(oldflow) - 1
        # No real lower bound as we have a guaranteed divergence point somewhere
        while oldflow[comparison_point] == new_flow[convergence_point]:
            convergence_point -= 1
            comparison_point -= 1
        # Add one to the convergence_point if it is not the last hop as it was decreased before the
        # divergence was observed
        # Remove on from the divergence as we want to include all routers that need to be modified,
        # thus this includes the last common one across both flows as it needs to be reconfigured
        # to forward traffic along the start of a new path
        return (divergence_point - 1,
                (convergence_point + 1 if convergence_point < len(new_flow) - 1 else convergence_point))


class PairedFlows(object):
    """A class holding all disjoint paths between two hosts in a network"""

    def __init__(self, network, n1, n2):
        self.network = network  # the nework graph
        self.n1 = n1  # a first host
        self.n2 = n2  # a second host
        self.all_flows = []  # all disjoint paths between them
        self.compute_paths()

    def compute_paths(self):
        """Compute successively each possible disjoint paths between n1 and n2."""
        net = self.network.copy()
        flow = path_selection(net, self.n1, self.n2)
        while flow is not None:
            # Find all possible paths between n1 and n2
            self.all_flows.append(Flow(self.network, flow))
            # That are disjoint
            net.remove_edges_from([(flow[i], flow[i + 1]) for i in range(len(flow) - 1)])
            flow = path_selection(net, self.n1, self.n2)

    def compute_backups(self):
        """Compute each backup flow for all selected flows between n1 and n2"""
        for flow in self.all_flows:
            flow.compute_backups()
            

def all_network_pairedflows(network):
    """Return a dictionary where each key is a distinct pair of hosts in the network,
    and where its value is a PairedFlows instance for the given network and hosts"""
    pairs = {}
    for n1 in network:
        for n2 in network:
            if n1 == n2:
                continue
            # compute all disjoint flows
            pairs[(n1, n2)] = PairedFlows(network, n1, n2)
    return pairs
