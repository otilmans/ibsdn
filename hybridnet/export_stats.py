#!/bin/env python
import os
from controller.packetparser import PacketParser
from controller.stat_tracker import Tracker

STATS_EXT = '.stats'


def main():
    parser = PacketParser('rendered/localhost/netkit/capture')
    files = os.listdir('.')
    for filename in files:
        if filename.endswith(STATS_EXT):
            tracker = Tracker.load(filename)
            print '%s -> loss rate: %s' % (filename, tracker.probe_loss_ratio(parser))
            tracker.export(filename, parser)

if __name__ == '__main__':
    main()