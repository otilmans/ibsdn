#!/usr/bin/env python
from controller.packetparser import PacketParser


if __name__ == '__main__':
    import sys
    if len(sys.argv) < 2:
        print 'Usage: %s [-i index] the directory containing the capture files,' \
            ' -i will display only probes after index' % sys.argv[0]
        sys.exit(0)
    parser = PacketParser(sys.argv[len(sys.argv) - 1],
                          0 if not '-i' in sys.argv else int(sys.argv[sys.argv.index('-i') + 1]))
    print parser.list_traces()