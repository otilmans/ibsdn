#!/usr/bin/env python
import argparse
import os
import errno
from compiler import CAPTURE_FOLDER


def parse_args():
    parser = argparse.ArgumentParser(description='Topology generator for the hybridnet controller'
                                                 'and the netkit testbed.')
    parser.add_argument('-i', '--input-graph', type=str, default=None,
                        help='An input .graphml file to compute the network topology')
    parser.add_argument('-r', '--rocketfuel', type=str, default=None,
                        help='A RocketFuel topology to compute the network layout')
    parser.add_argument('-t', '--tcpdump', type=bool, default=True,
                        help='Save all traffic to pcap files')
    parser.add_argument('-f', '--filter-dump', type=bool, default=True,
                        help='Restrict the saved packets to only the probe packets')
    return parser.parse_args()


def main():
    args = parse_args()
    network = None
    if args.input_graph:
        from compiler import graphml
        network = graphml.compile_graphml(args)
    elif args.rocketfuel:
        from compiler import rocketfuel
        network = rocketfuel.compile_rocketfuel(args)
    if not network:
        print 'No input topology, please rerun the program with -h/--help if you need usage information.'
    else:
        from compiler.network.net import save

        print 'Compiled a network of %d hosts' % len(network)
        print network
        out = os.path.splitext(os.path.basename(args.input_graph))[0]
        save(network, out)
        try:
            os.makedirs(CAPTURE_FOLDER)
        except OSError as exc:  # Python >2.5
            if exc.errno == errno.EEXIST and os.path.isdir(CAPTURE_FOLDER):
                pass
            else:
                raise exc
        print '======================================================================='
        print 'The network has been compiled.'
        print 'The Netkit lab is under $PWD/rendered/localhost/netkit'
        print 'The network topology input for the controller is at $PWD/%s' % ('%s.net' % out)
        print '======================================================================='


if __name__ == '__main__':
    main()