% if node.zebra:
!
## Loopback
interface lo
description local loopback
ip address 127.0.0.1/8
ip address ${node.loopback}/32
!


log file /var/log/zebra/zebra.log
%endif