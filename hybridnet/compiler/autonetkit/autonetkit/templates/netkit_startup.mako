#!/bin/bash
% for i in node.interfaces:
/sbin/ifconfig ${i.id} ${i.ipv4_address} netmask ${i.ipv4_subnet.netmask} broadcast ${i.ipv4_subnet.broadcast} up
% endfor
route del default
/sbin/ifconfig lo 127.0.0.1 up
/etc/init.d/ssh start
/etc/init.d/hostname.sh
% if node.zebra:
/etc/init.d/zebra start
% endif
% if node.bird:
mkdir /run/bird
bird -c /etc/bird.conf
sysctl net.ipv4.ip_forward=1
% endif
% if node.openvswitch:
/sbin/modprobe openvswitch
mkdir -p /usr/etc/openvswitch
ovsdb-tool create /usr/etc/openvswitch/conf.db /root/openvswitch/vswitchd/vswitch.ovsschema
mkdir -p /var/run/openvswitch
ovsdb-server --remote=punix:/var/run/openvswitch/db.sock --remote=db:Open_vSwitch,Open_vSwitch,manager_options --private-key=db:Open_vSwitch,SSL,private_key --certificate=db:Open_vSwitch,SSL,certificate --bootstrap-ca-cert=db:Open_vSwitch,SSL,ca_cert --pidfile --detach
ovs-vsctl --no-wait init
ovs-vswitchd --pidfile --detach
BR="br0"
CHECKBR=$(ovs-vsctl list-br)
[[ "$BR" = "$CHECKBR" ]] && echo "Destroying previous OVS bridge" && ovs-vsctl del-br br0
ovs-vsctl add-br br0
% for i in node.interfaces:
% if i.description != 'Loopback':
ovs-vsctl add-port br0 ${i.id}
/sbin/ifconfig ${i.id} promisc up
% if node.tcpdump:
HWADDR=`cat /sys/class/net/${i.id}/address`
tcpdump -U -i ${i.id} -w /hostlab/${node.capture_dir}/${node.node_id}-${i.id}-$HWADDR-0.pcap ${node.tcpdump_filter}&
% endif
% endif
% endfor
ovs-vsctl set bridge br0 protocols=OpenFlow13
ovs-vsctl -- set Bridge br0 other_config:datapath-id=${node.dpid}
ovs-vsctl set-controller br0 tcp:${node.controller_ip}:${node.controller_port}
ovs-vsctl set-port-normal-mode br0 kernel
% endif
/etc/init.d/inetd restart
echo pts/0 >> /etc/securetty
echo pts/1 >> /etc/securetty
echo pts/2 >> /etc/securetty
echo pts/3 >> /etc/securetty
echo pts/4 >> /etc/securetty
echo pts/5 >> /etc/securetty
echo pts/6 >> /etc/securetty
% if node.default_gw:
route add default gw ${node.default_gw}
% endif