protocol kernel {
        persist no;       # Do not persist routes when bird is stopped
        learn no;         # Do not learn alien routes from the kernel
        export all;       # Export all learnt routes to the kernel
        import none;      # Do not import any alien route from the kernel
}

protocol device {
        scan time 10;           # Scan the interfaces often
        % for itf in node.interfaces:
        primary "${itf.id}" 0.0.0.0;
        % endfor
}

protocol ospf ${node.name} {
        rfc1583compat yes;
        area 0.0.0.0 {
                networks {
                        % for ospf_link in node.ospf.ospf_links:
                        ${ospf_link.network.cidr};
                        % endfor
                };
                % for itf in node.interfaces:
                %if itf.ospf_cost:
                interface "${itf.id}" {
                        cost ${itf.ospf_cost};
                        hello 1;
                        retransmit 1;
                        wait 1;
                        dead 3;
                        authentication none;
                        type broadcast;
                };
                % endif
                % endfor
                interface -"br0*";
        };
}