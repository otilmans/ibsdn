from networkx import Graph, is_connected
import cPickle as pickle

EXTENSION = '.net'


def network_archive_name(filename):
    return filename if filename.endswith(EXTENSION) else \
        '%s%s' % (filename, EXTENSION)


class Network(object):
    FLOW_COOKIE = 0

    def __init__(self):
        self.graph = Graph()
        self.dpid_id = {}  # dpid -> id
        self.flows = {}
        self.backup_flows = {}

    def add_backup_flow(self, cookie, flow):
        self.backup_flows[cookie] = flow

    def clear_backup_flows(self):
        self.backup_flows.clear()

    def add_flow(self, flow):
        Network.FLOW_COOKIE += 1
        self.flows[Network.FLOW_COOKIE] = flow
        for node in flow:
            self.node(node).add_flow(Network.FLOW_COOKIE, flow)
        return Network.FLOW_COOKIE

    def remove_flow(self, cookie):
        for node in self.flows[cookie]:
            self.node(node).remove_flow(cookie)
        del self.flows[cookie]

    def add_node(self, id, node):
        self.graph.add_node(id, node=node)
        self.dpid_id[node.dpid] = id

    def add_edge(self, src, dst, cost=1, **kwargs):
        self.graph.add_edge(src, dst, ospf_cost=cost, **kwargs)

    def neighbors(self, id):
        """Generator over each Node linked to this one"""
        for n in self.graph.neighbors_iter(id):
            yield self.node(n)

    def node(self, id):
        """Node corresponding to id"""
        try:
            return self.graph.node[id]['node']
        except:
            return None

    def id_for(self, dpid):
        return self.dpid_id[dpid]

    def dpid(self, dpid):
        """Node corresponding to dpid"""
        return self.node(self.id_for(dpid))

    def edge(self, src, dst):
        """Edge between src and dst, these are id's not dpid's!"""
        return self.graph.get_edge_data(src, dst)['edge']

    def links(self):
        return [link for link in self.graph.edges_iter()]

    def __len__(self):
        return len(self.dpid_id)

    def __iter__(self):
        return self.graph.nodes_iter()

    def nodes(self):
        for n, data in self.graph.nodes_iter(data=True):
            yield data['node']

    def __repr__(self):
        return '\n '.join([str(n) for n in self.nodes()])

    def __getstate__(self):
        state = dict()
        state['nodes'] = [n for n in self.nodes()]
        state['links'] = [{'src': src, 'dst': dst.id, 'data': data}
                          for src, dst, data in self.graph.edges_iter(data=True)]
        return state

    def __setstate__(self, state):
        self.__init__()
        for n in state['nodes']:
            self.add_node(n.id, n)
        for l in state['links']:
            self.add_edge(**l)

    def update_port_liveness(self, dpid, port_no, is_live):
        """@see Node.update_port_liveness"""
        return self.dpid(dpid).update_port_liveness(port_no=port_no, is_live=is_live)

    def port_str(self, dpid, port_no):
        """@see Node.port_str"""
        return self.dpid(dpid).port_str(port_no)

    def register_dp(self, dp):
        """Registers the Ryu datapath object to its corresponding node
        Returns true if all switches have now connected"""
        self.dpid(dp.id).dp = dp
        for n in self.nodes():
            if not n.dp:
                return False
        return True

    def unregister_dp(self, dp):
        self.dpid(dp.id).dp = None

    def register_link(self, src_dpid, src_port, dst_dpid, dst_port):
        src = self.dpid(src_dpid)
        dst = self.dpid(dst_dpid)
        src_itf = src.interface_for_dst(dst.id)
        if src_itf:
            src_itf.of_portno = src_port
            dst_itf = src_itf.bound_interface_for(dst.id)
            dst_itf.of_portno = dst_port
            src.ofport_eth[src_port] = src_itf.id
            dst.ofport_eth[dst_port] = dst_itf.id
        else:
            # No link found, ... not supposed to be reached
            print 'Unknow link: %s[%s] -> %s[%s]' % (src_dpid, src_port,
                                                     dst_dpid, dst_port)

    def can_remove_link(self, n1, n2):
        g = self.graph.copy()
        g.remove_edge(n1, n2)
        return is_connected(g)

    def flows_with_link(self, src, dst, directed=False):
        for cookie, flow in self.node(src).flows.iteritems():
            if flow.contains_link(src, dst, directed):
                yield cookie, flow


def load(filename):
    src = network_archive_name(filename)
    print 'Loading the network located at %s' % src
    with open(src, 'r') as f:
        network = pickle.load(f)
    print 'Loaded a network of %s nodes' % len(network)
    return network


def save(network, target):
    dest = network_archive_name(target)
    print 'Saving the network as %s' % dest
    with open(dest, 'w') as f:
        pickle.dump(network, f)
