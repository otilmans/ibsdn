class Interface(object):
    def __init__(self, node, id, ip):
        self.node = node
        self.id = id
        self.bound_to = []
        self.ip = ip
        self.hwaddr = None
        self.of_portno = None
        self.node.interfaces[self.id] = self
        self.live = True

    def __repr__(self):
        return '<%s|%s|%s@%s%s>' % (self.id, self.of_portno, self.hwaddr, self.ip,
                                    '-> %s' % ', '.join('%s/%s' %(str(i.node.id), str(i.id))
                                                        for i in self.bound_to)
                                    if len(self.bound_to) > 0 else '-')

    def can_reach(self, dst_id):
        return not (self.bound_interface_for(dst_id) is None)

    def bound_interface_for(self, dst_id):
        for other_itf in self.bound_to:
            if other_itf.node.id == dst_id:
                return other_itf
        return None

    def port_str(self):
        return self.node.port_str(self.of_portno)


class Node(object):
    def __init__(self, id, dpid=None, tap_ip=None):
        self.id = id
        self.dpid = dpid
        self.tap_ip = tap_ip
        self.interfaces = {}  # ethX -> Interface
        self.ofport_eth = {}  # ofport_no -> ethX
        self.flows = {}
        self.dp = None

    def add_flow(self, cookie, flow):
        self.flows[cookie] = flow

    def remove_flow(self, cookie):
        del self.flows[cookie]

    def __getstate__(self):
        # We don't want to save the Ryu specific datapath instances in the pickle file
        state = self.__dict__.copy()
        del state['dp']
        return state

    def __setstate__(self, state):
        # Ensures we have a ivar named dp after unpickling
        self.__dict__.update(state)
        self.dp = None

    def __repr__(self):
        return '[%s|%s@%s | \n\t%s\n\t]' % (self.id, self.dpid, self.tap_ip,
                                            '\n\t'.join([str(i) for i in self.interfaces.itervalues()]))

    def port(self, port_no):
        return self.interfaces[self.ofport_eth[port_no]]

    def update_port_liveness(self, port_no, is_live):
        """Update the liveness status of a given port and return whether this was old news
        or not"""
        itf = self.port(port_no)
        was_live = itf.live
        itf.live = is_live
        return not(was_live == is_live)

    def port_str(self, port_no):
        """Return a string describing the port on this node"""
        return '%s/%s' % (self.id, self.port(port_no).id)

    def interfaces_iter(self):
        for itf in self.interfaces.values():
            yield itf

    def interface_for_dst(self, dst_id):
        for itf in self.interfaces.values():
            if itf.can_reach(dst_id):
                return itf
        return None
