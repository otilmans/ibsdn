from controller.probe import ProbePacket
from network.net import Network
from network.node import Node, Interface
from compiler import CAPTURE_FOLDER


def dp_id(id):
    return format(id, '16x').replace(' ', '0')


def base_anm(graph):
    from autonetkit.autonetkit.build_network import (initialise, build_phy,
        build_l3_connectivity)
    from autonetkit.autonetkit.design.osi_layers import (build_layer2,
        check_layer2, build_layer2_broadcast, build_layer3)
    anm = initialise(graph)
    g_in = anm['input']
    g_in.update(g_in.nodes(platform="netkit"), syntax="bird")
    g_in.update(g_in.switches(), openvswitch=True)
    g_in.update(g_in.routers(), openvswitch=True)
    build_phy(anm)
    build_layer2(anm)
    check_layer2(anm)
    build_layer2_broadcast(anm)
    build_layer3(anm)
    build_l3_connectivity(anm)
    anm['phy'].data.enable_routing = True
    return anm


def set_default_gw(server):
    ip = (None, None)
    for itf in server.neighbor_interfaces():
        if itf.node.is_router():
            ip = (itf.node.id, itf.interface_id)
            break
    server.default_gw = ip


def build_ip(anm):
    from autonetkit.autonetkit.design.ip import build_ip, build_ipv4

    build_ip(anm)
    build_ipv4(anm, infrastructure=True)
    g_phy = anm['phy']
    g_phy.update(g_phy, use_ipv4=True)


def build_igp(anm):
    g_in = anm['input']
    default_igp = g_in.data.igp or "ospf"
    from autonetkit.autonetkit import ank

    ank.set_node_default(g_in, igp=default_igp)
    from autonetkit.autonetkit.design.igp import build_ospf

    build_ospf(anm)


def build_anm(f):
    anm = base_anm(f)
    for s in anm['phy'].servers():
        set_default_gw(s)
    build_ip(anm)
    build_igp(anm)
    return anm


def render_network(anm, tcpdump, filterdump):
    from autonetkit.autonetkit.console_script import create_nidb
    from autonetkit.autonetkit.compilers.platform.netkit import NetkitCompiler
    from autonetkit.autonetkit.render import render
    nidb = create_nidb(anm)
    compiler = NetkitCompiler(nidb, anm, 'localhost')
    compiler.compile()
    g_phy = anm['phy']
    for s in nidb.servers():
        (n, i) = g_phy.node(s.node_id).default_gw
        ip = nidb.node(n).interface(i).ipv4_address
        print 'Setting default gateway of %s to %s[%s]' % (s.id, n, ip)
        s.default_gw = ip
    dpid = 1
    for d in nidb.routers():
        d.bird = True
        d.zebra = False
        d.openvswitch = True
        d.dpid = dp_id(dpid)
        dpid += 1
    for n in nidb.nodes():
        n.tcpdump = tcpdump
        n.tcpdump_filter = "'udp src port 42'" if filterdump else \
                           "'not ip proto 89'"
        n.capture_dir = CAPTURE_FOLDER
    render(nidb)
    for r in nidb.routers():
        r.dpid = int(r.dpid, 16)
    return nidb


def compile_network(network, tcpdump=True, filterdump=True):
    """Given a networkx graph, build a network object"""
    anm = build_anm(network)
    nidb = render_network(anm, tcpdump, filterdump)
    net = Network()
    g_phy = anm['ospf']
    for node in g_phy:
        nidb_node = nidb.node(node.id)
        router = Node(id=node.id, dpid=nidb_node.dpid, tap_ip=nidb_node.tap.ip)
        net.add_node(node.id, router)
        for itf in nidb_node.physical_interfaces():
            Interface(router, itf.id, itf.ipv4_address)
    for edge in g_phy.edges():
        net.add_edge(edge.src, edge.dst, cost=edge.cost)
    
    get_itf = lambda itf: net.node(itf.node.id).interfaces[itf.id]
    cd_to_intf = {}
    for cd in nidb.nodes():
        for itf in cd.physical_interfaces():
            for edge in itf.edges():
                dst = edge.dst_id
                node = nidb.node(dst)
                if node.is_l3device() or node.is_switch():
                    get_itf(itf).bound_to.append(get_itf(edge.dst_int))
                if dst not in cd_to_intf:
                    cd_to_intf[dst] = []
                cd_to_intf[dst].append(itf)
    # chain all interfaces belonging to the same cd together
    for interfaces in cd_to_intf.itervalues():
        for itf in interfaces:
            main_itf = get_itf(itf)
            for other in interfaces:
                if other is itf:
                    continue
                main_itf.bound_to.append(get_itf(other))
    return net


def compile_graphml(args):
    from autonetkit.autonetkit.build_network import load

    print 'Computing network from graphml file'
    with open(args.input_graph, "r") as fh:
        input_file = fh.read()
    graph = load(input_file)
    return compile_network(graph, args.tcpdump, args.filter_dump)
