from networkx import Graph


def clean_host(name):
    return name.translate(None, '+,')


def add_link(net, link):
    """Parses and add a link from a rocketfuel weighted topology"""
    parts = link.split(' ')
    if len(parts) == 3:
        src = clean_host(parts[0])
        dst = clean_host(parts[1])
        cost = float(parts[2])
        net.add_edge(src, dst, ospf_cost=cost)
    else:
        print 'unknown link format: "%s"' % link


def load_weighted_igp(path):
    """Return a weighted graph from a rocketfuel weighted topology"""
    network = Graph()
    with open(path, 'r') as topology:
        for line in topology:
            add_link(network, line.strip())
    for node, node_data in network.nodes_iter(data=True):
        node_data['device_type'] = 'router'
        node_data['label'] = node
    return network


def compile_rocketfuel(args):
    """Compile a rocketfuel topology into a network object"""
    from graphml import compile_network

    graph = load_weighted_igp(args.rocketfuel)
    return compile_network(graph, args.tcpdump, args.filter_dump)