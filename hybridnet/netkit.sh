#!/bin/sh

LAB=rendered/localhost/netkit

function stop() {
    lcrash -d ${LAB} || true
    lclean -d ${LAB} || true
    killall netkit-kernel &>/dev/null 2>/dev/null
}

function start() {
    stop
    MODULE='tun'
    if lsmod | grep "$MODULE" &> /dev/null ; then
      # refresh sudo rights ... might need it to startup netkit with tap interfaces
      sudo echo -n
    else
      echo "Loading up the required module & setting iptables"
      sudo modprobe $MODULE
      sudo iptables -I OUTPUT -o nk_tap_$USER -j ACCEPT
      sudo iptables -I INPUT -i nk_tap_$USER -j ACCEPT
      sudo iptables -I FORWARD -i nk_tap_$USER -j ACCEPT
      sudo iptables -I FORWARD -o nk_tap_$USER -j ACCEPT
    fi
    mkdir -p ${LAB}/capture
    rm -f ${LAB}/capture/*.pcap
    lstart -p3 -d ${LAB}
    echo '0' > PROBE_ID
}

if [ $# -eq 0 ]
then
    start
else
    case $1 in
    stop) stop;;
    ?) start;;
    esac
fi